# Unity Review
***

## Code  
### Правила написания кода  

#### Порядок кода 
В классе, стуруктуре или интерфейсе:  

* Enums
* Constant Fields
* Fields
* Constructors
* Finalizers (Destructors)
* Delegates
* Events
* Interfaces (interface implementations)
* Methods
* Properties
* Indexers
* Structs
* Classes  

Сортировка по модификаторам доступа: 

* public
* internal
* protected internal
* protected
* private  

Далее сортировка по static, non-static: 

* static
* non-static  

Далее сортировка по readonly:  

* readonly
* non-readonly  

#### Всегда указывать модификаторы доступа  
```csharp
// Bad
void Update(){ }
    
// Good
private void Update(){ }
```
	
#### Выносить магические значения *(ифры, строки и т.д.)* в константы  
```csharp
private const sting IdleAnimationName = "Idle";
    
private Animator _animator;
    
private void Update(){
    // Bad
    _animator.Play("Idle");
        
    // Good
    _animator.Play(IdleAnimationName)
}
```

#### Transform  
При работе с **Transform** лучше пользоваться `transform.localPosition` и `transform.localRotation`, так как это обычные свойства. В то время как изменение `transform.position` и `transform.rotation` вызывают `OnTransformChanged`, а это дорогая операия.
Так же если нужно изменить несколько значений `transform.position` например **X** и **Y**, то лучше делать это одновременно.  
```csharp
private void ChangePosition(float x, float y){
    // Bad
    transform.position = new Vector3(x, transfrom.x, transfrom.z);
    transform.position = new Vector3(transfrom.y, y, transfrom.z);
    
    // Good
    transform.position = new Vector3(x, y, transfrom.z);
    
    // Better
    Vector3 position = tramsform.position;
    position.x = x;
    position.y = y;
    transfrom.position = position;
}
```  
  
### Кеширование ссылок  
1. Все вызовы *(по возможности)* метода `GetComponent()` должны сохраняться в переменные, для последующего использования. Данный подход поможет избежать многократного вызова метода `GetComponent()` который является крайне затратным.
2. Кеширование метода `GetComponent()` стоит проводить в методах инициализации `Awake()` или `Start()`, но не в коем случае их не стоит делать в `Update()`, `FixedUpdate()`, `LateUpdate()`.
3. `Camera.main` при своей локоничности и простоте, является ловушкой, котороя под капотом делает `FindGameObjectsWithTag()`, что в свою очередь тоже является крайне затратной операцией.  
```csharp
using UnityEngine;
using System.Collections;

public class ExampleClass : MonoBehaviour{
    private Camera cam;
    private CustomComponent comp;

    void Start(){
        cam = Camera.main;
        comp = GetComponent<CustomComponent>();
    }

    void Update(){
        // Good
        this.transform.position = cam.transform.position + cam.transform.forward * 10.0f;

        // Bad
        this.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 10.0f;

        // Good
        comp.DoSomethingAwesome();

        // Bad
        GetComponent<CustomComponent>().DoSomethingAwesome();
    }
}
```  

Примечание:  
> Отказ от использования `GetComponent(string)`  
> Поиск по строке в сцене значительно дороже, чем поиск по типу.  
> **(Хорошо)** Component GetComponent(Type type)  
> **(Хорошо)** T GetComponent<T>()  
> **(Плохо)** Component GetComponent(string)  

#### RequireComponent()  
При кешировании компонента с помощью `GetComponent()` в методах `Awake()` и `Start()` хорошей практикой считается использование атрибута `[RequireComponent()]`. Который автоматически добавляет нужный компонент, если он отсутствует, в момент добавления нашего скрипта на объект.
```csharp
[RequireComponent(typeof(RigidBody))]
public class Foo{
    private RigidBody _someRb;
    
    private void Awake(){
        _someRb = GetComponent<RigidBody>();
    }
}
```  

### Дорогостоящие операции  
#### Отказ от использования LINQ  
На данный момент **06.08.2020** использование LINQ резервирует очень большое количество памяти, так использование является нежелательным. 
> В **2021 году** Unity планирует интегрировать новую архитектуру **DOTS** которая вроде бы должна решить проблему описаную выше.
  
#### API Unity  
Некоторые **API Unity** при всей очевидной пользе могут быть очень ресурсоемкими. Большинство из них выполняют поиск соответствующих игровых объектов по всему графу сцены. Обычно этих операций можно избежать, кэшируя ссылки или создав компонент управления нужными игровыми объектами для отслеживания ссылок во время выполнения.

> *GameObject.SendMessage()*,  
> *GameObject.BroadcastMessage()*,  
> *UnityEngine.Object.Find()*  
> *UnityEngine.Object.FindWithTag()*  
> *UnityEngine.Object.FindObjectOfType()*  
> *UnityEngine.Object.FindObjectsOfType()*  
> *UnityEngine.Object.FindGameObjectsWithTag()*  
> *UnityEngine.Object.FindGameObjectsWithTag()*  
  
#### Упаковка значений  
***Упаковкой*** называют процесс создания переменных со ссылочным типом в виде оболочек для переменных со значениями типа, такими как char, int, bool и т. д. Если переменная со значением типа "упакована", она помещается в объект System.Object, который хранится в управляемой куче. Это означает, что для нее выделяется память и ее нужно обрабатывать сборщиком мусора после освобождения. Такие процессы выделения и освобождения ресурсов приводят к снижению производительности, а во многих случаях их можно убрать или легко заменить на менее дорогостоящую альтернативу.

Одной из наиболее распространенных форм упаковки-преобразования является использование типов значений, допускающих значение ***NULL***. Часто вам нужно, чтобы функция могла возвращать значение ***NULL*** для типа значения, особенно если операция не всегда может получить нужное значение. Потенциальная проблема этого подхода заключается в том, что выделение памяти выполняется из кучи и должно затем обрабатываться сборщиком мусора.  
```csharp
// boolean value type is boxed into object boxedMyVar on the heap
bool myVar = true;
object boxedMyVar = myVar;

// Example of function returning nullable value type
public int? TryGetSpeed(){
    // Returns current speed int value or null if fails
}
```
  
### Повторяющиеся пути кода  
Любые повторяющиеся функции обратного вызова в Unity (например, Update), которые выполняются много раз каждую секунду и (или) для каждого кадра, следует обдумывать очень тщательно. Любые дорогостоящие операции в этом коде будут иметь огромное и стабильное влияние на производительность.
  
#### Пустые функции обратного вызова  
Несмотря на внешнюю безобидность приведенного ниже кода, эти пустые обратные вызовы могут быть очень ресурсоемкими, особенно учитывая, что каждый скрипт Unity выполняет автоматическую инициализацию с помощью этого блока кода. Unity придется постоянно переключаться между неуправляемой и управляемой частью кода, то есть между кодом UnityEngine и кодом приложения. Такое переключение контекста на этой границе требует много ресурсов, даже если не выполняется никаких действий. Это особенно проблематично, если приложение использует сотни игровых объектов, компоненты которых имеют повторяющиеся пустые обратные вызовы Unity.
```csharp
// Bad
void Update(){ }
```
  
> Методы на которые стоит обратить внимание:
> *Update()*  
> *FixedUpdate()*  
> *LateUpdate()*  
> *OnPostRender()*  
> *OnPreRender()*  
> *OnRenderImage()*  
> и т. п.
  
#### Однократное выполнение операций для каждого кадра  
Хотя и не всегда, результаты некоторых функций часто можно вычислять один раз за кадр и повторно использовать в приложении для всего этого кадра. Чаще всего создается ***один объект (возможно Singlton)*** который вызывает эти функции и хранит их в себе, а остальные объекты посредством общщения с ним получают нужные им данные.

> К таким функиям можно отнести:  
> UnityEngine.Physics.Raycast()  
> UnityEngine.Physics.RaycastAll()  
> UnityEngine.Object.GetComponent()  
> UnityEngine.Object.Instantiate()
  
#### Интерфейсы и виртуальные конструкции  
Вызов функций через интерфейсы вместо прямого обращения к объектам либо вызов виртуальных функций часто будет гораздо более затратным, чем использование прямых конструкций или прямых вызовов функций. Если виртуальная функция или интерфейс являются необязательными, их следует удалить. Однако для этих подходов ухудшение производительности часто будет хорошим компромиссом, если их применение упрощает совместную работу, повышает удобочитаемость или удобство поддержки кода.

В общем случае рекомендуется не объявлять поля и функции виртуальными, если нет четкого понимания, что этот элемент придется перезаписывать.
  
#### Передача структур по значению  
В отличие от классов, для структуры типы определяются значением, и при прямой передаче в функцию их содержимое копируется в только что созданный экземпляр. Поэтому внутри функии вы будете работать с ***другим*** объектом и ***любые изменения будут применятся к нему, а не к внешнему объекту переданому в эту функцию***. Также эта копия повышает нагрузку на ЦП и требует дополнительной памяти из стека. Влияние небольших структур обычно минимально и может считаться приемлемым. Но для тех функций, которые несколько раз вызываются для каждого кадра и (или) принимают крупные структуры, по возможности измените определение функции так, чтобы передавать им ссылку.  
```csharp
class TheClass
{
    public string willIChange;
}

struct TheStruct
{
    public string willIChange;
}

class TestClassAndStruct
{
    static void ClassTaker(TheClass c)
    {
        c.willIChange = "Changed";
    }

    static void StructTaker(TheStruct s)
    {
        s.willIChange = "Changed";
    }

    static void Main()
    {
        TheClass testClass = new TheClass();
        TheStruct testStruct = new TheStruct();

        testClass.willIChange = "Not Changed";
        testStruct.willIChange = "Not Changed";

        ClassTaker(testClass);
        StructTaker(testStruct);

        Console.WriteLine("Class field = {0}", testClass.willIChange);
        Console.WriteLine("Struct field = {0}", testStruct.willIChange);
    }
}
/* Output:
    Class field = Changed
    Struct field = Not Changed
*/
```

#### Vector3 и Vector2  
Арифметические операции между труктурами **Vector3** и **Vector2** являются более сложными и ресурсозатратными нежели **int + int**, так что нужно постараться сократить их как можно сильнее.  
```csharp
private Vector3 Calculate(Vector3 someVector){
    // Bad
    var foo = someVector * 2 / 3 * 10;
    
    //Good
    var foo = someVector * (2 / 3 * 10);
    
    return foo;
}
```
  
### Прочее  
#### Физика  
+ Как правило, для улучшения обработки физики следует ограничить количество времени или количество итераций в секунду, затрачиваемых на ее обработку. Разумеется, это снизит точность имитации.  
```csharp
    private const int PhysicsUpdateFrequency = 3;
    
    private void FixedUpdate(){
        if(Time.frameCount % PhysicsUpdateFrequency == 0){
            transform.position = ...;
        }
    }
```  
+ Тип коллайдера в Unity может иметь очень разные характеристики производительности. В приведенном ниже списке коллайдеры перечислены в порядке уменьшения производительности, слева направо. Важнее всего избегать коллайдеров сетки, которые значительно дороже коллайдеров примитивов.
    >  Sphere < Capsule < Box <<< Mesh (Convex) < Mesh (non-Convex)

  
#### Анимаия  
Избавьтесь от анимации простоя, отключив компонент Animator (отключение объекта Game не дает такого эффекта). Избегайте шаблонов разработки, в которых аниматор в цикле постоянно присваивает значение одному параметру. Такая техника влечет значительные издержки без видимого эффекта в приложении.  
```csharp
private const sting IdleAnimationName = "Speed";
private const int MaxCharacterSpeed = 1;
    
private Animator _animator;
    
private int _speed;
 
private void Update(){
    // Bad
    _animator.SetInt(IdleAnimationName, _speed);
}
        
private void OnCharacterSpeedChange(int speed){
    _speed = speed;
        
    // Good
     _animator.SetInt(IdleAnimationName, speed);
}
        
private void TurnOffAnimation(){
    // Bad
    gameObject.SetActive(false);
        
    // Good
    _animator.enabled = false;
}
```
  
#### Сбор мусора  
При разработке в Unity одной из самых распространенных ошибок, которые приводят к избыточному сбору мусора, является отсутствие кэша для ссылок на компоненты и классы. Все ссылки следует собирать в методе `Start()` или `Awake()` и всегда использовать повторно из кэша в функциях `Update()` и `LateUpdate()`.

Еще немного советов:

* Используйте класс C# `StringBuilder` для динамического создания сложных строк в процессе выполнения.
* Удалите все вызовы `Debug.Log()`, когда в них отпадет необходимость, иначе они будут по-прежнему выполняться в рабочих версиях приложения.
* Если приложение часто требует большого объема памяти, попробуйте вызывать `System.GC.Collect()` на этапах загрузки, например при отображении экрана загрузки или перехода между сценами.
* Использовать object pool
* Если вы точно уверены, что объект вам больше не понадобится, то можно убрать все ссылки на него `someObj = null`, благодаря этому при следующем проходе сборщика мусора данный объект будет успешно удален из кучи.  
* `gameObject.name` и `gameObject.tag` создают ненужный мусор, так как возвращают ***string***, значение нужно кешировать или использовать `gameObject.CompareTag()`  
* Корутины
```csharp
private IEnumerator SomeCoro(){
    // Bad, creates garbage
    yield return 0;
    
    // Good
    yield return null;
    
    // Bad, creates a lot of garbage
    while(isSelected){
        yield return new WaitForSeconds(1f);
    }
    
    // Good
    var wait = new WaitForSeconds(1f)
    while(isSelected){
        yield return wait;
    }
}
```  
* Избегать создание структур имеющие переменные ссылочного типа, так как сборщику мусора придется проверять их при каждом проходе. Если будет коллекция таких структур это может очень негативно повлиять на работоспособность приложения.  
```csharp
public struct Foo{
    public int bar;
    // Bad
    public string someStr;
}

// Very Bad
private Foo[] _fooArray;

// Better
private int[] _barArray;
private string[] _someStrArray;
```
  
***

## Inspector

### Hierarcy
Для удобства стуктурирования иерархии можно создавать объекты типа `----UI----`, которые помогают визуально разделить иерархию на части.
Для того что бы они не попадали в билд, этим объектам нужно выставить `tag` - `EditorOnly`. Еще желательно выставить дефолтные значения для transform, но это не обязательно.
![picture](https://bitbucket.org/apelih24/alphanova-rules/raw/master/img/1.jpg)

### UI
Основные способы оптимизации, более подробно по данной теме можно прочесть перейдя по 3 ссылке:  

* Отключать невидимые и прозрачные объекты
* Минимизировать количество элементов
* Избегать пересечения объектов, не способных запечься друг с другом
* Распределять элементы по холстам в зависимости от частоты обновления
* У холстов с часто обновляющимися элементами отключить Pixel Perfect
* Отключить Raycast Target у не кликабельных элементов
* Удалить компонент Graphic Raycaster на холсте, у которого все элементы не кликабельны
* Для обычных текстовых компонентов не использовать Best Fit
* Для TextMeshPro В World Space использовать TextMeshPro вместо TextMeshProUGUI. TextMeshProUGUI используется в холстах
* Устанавливать камеру в настройках холста, если надо
* Отключать холсты через отключение компонента холста
* Использовать спрайты с градацией серого и раскрашивать настройками компонента изображения
* Использовать спрайтовые атласы
* Использовать правильный размер и формат сжатия для текстур, не попавших в атлас

### Физика

### Spine

***

> После всех проверок и исправлений рекомендуется воспользоваться **Unity Profiler**

### Rider
1. Скачать архив configRider.rar, распаковать содержимое в отдельную папку.
2. Найти папку с настройками Rider. Для Windows путь будет примерно такой C:\Users\User-work\.Rider2019.3\config
3. Папку config со своими настройками можно скопировать и сохранить в любом удобном месте
4. Скопировать содержимое папки config из архива в папку ...\.Rider2019.3\config с заменой файлов
5. Запустить Rider, открыть окно настроек (File -> Settings)
![picture](https://bitbucket.org/apelih24/alphanova-rules/raw/master/img/2.jpg)
6. В нижнем левом углу окна настроек нажать кнопку Manage Layers
![picture](https://bitbucket.org/apelih24/alphanova-rules/raw/master/img/3.jpg)
7. В появимшемся окне Settings Layers выбрать строчку This Computer
![picture](https://bitbucket.org/apelih24/alphanova-rules/raw/master/img/4.jpg)
8. В том же окне жмем кнопку Import from (см. скрин) и находим в проводнике файл CodeStyleSettings.DotSettings и повторяем для файла GlobalSettingsStorage.DotSettings
![picture](https://bitbucket.org/apelih24/alphanova-rules/raw/master/img/5.jpg)

## Полезные ссылки
1. https://docs.microsoft.com/ru-ru/windows/mixed-reality/performance-recommendations-for-unity, подсказки по оптимизации кода для Unity от Microsoft
2. https://learn.unity.com/tutorial/fixing-performance-problems#5c7f8528edbc2a002053b594, гайд по работе с памятью от Unity
3. https://temofeev.ru/info/articles/optimizatsiya-unity-ui/, подсказа по оптимизаии UI
4. https://rob-bell.net/2009/06/a-beginners-guide-to-big-o-notation/, юзер френдли статья про сложность алгоритмов
5. https://blogs.unity3d.com/2017/06/29/best-practices-from-the-spotlight-team-optimizing-the-hierarchy/, о том, что плохо делать глубокие иерархии.
6. https://easings.net/, материал по функциям плавности(для движения и прочего)